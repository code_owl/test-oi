1. Create a local folder with virtual environment inside. # virtualenv -p python3 . (if you use globaly python2)
2. Clone and add to folder with virtualenv folder with the project.
3. Activate your virtual virtual environment via command source bin/activate
4. Install all packages requirements.txt file via command pip install -r requirements.txt
5. Edit settings.py file inside the project for your database.
6. Apply migrations for the project via command python manage.py migrate
7. Create super user of the project via command python manage.py createsuperuser
8. Download the fixtures via command python manage.py loaddata bikes/fixtures/all.json
9. Run the server via comman python manage.py runserver